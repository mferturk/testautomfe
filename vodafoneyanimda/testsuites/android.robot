*** Settings ***
Test Setup          launch app with device id
Test Teardown       close_app
Library             AppiumLibrary
#Library             AnywhereLibrary
#Library             FakerLibrary
Library             Collections
Library             String
Library             Screenshot
#Library             ../resources/externalKeywords.py
Resource            ../resources/mfeKeywords.robot
Resource            ../resources/vodafoneYanimda_functions.robot
Resource            ../resources/vodafoneYanimda_object_repository_android.robot
Resource            ../resources/vodafoneYanimda_object_repository_ios.robot
#Resource            ../resources/vodafoneYanimda_variables.py
Resource            ../resources/test_datas.robot

*** Test Cases ***
#1. permissions
#    [documentation]  this test case is going to be moved another test suite that includes only reinstallation required cases
#    check permissions

#2. accept user agreement
#    [documentation]  this test case is going to be moved another test suite that includes only reinstallation required cases
#    allow phone calls
#    allow access media
#    accept user agreement

#3. reject user agreement
#   [documentation]  this test case is going to be moved another test suite that includes only reinstallation required cases
#    allow phone calls
#    allow access media
#    reject user agreement

4. login page
    ${loginstatus}  wait for homepage
    verify user not logged in   ${loginstatus}
    check login page

5. help page
    ${loginstatus}  wait for homepage
    verify user not logged in   ${loginstatus}
    go to help page
    check help page

6. free usage info
    ${loginstatus}  wait for homepage
    verify user not logged in   ${loginstatus}
    go to help page
    check free usage info

7. auto login info
    ${loginstatus}  wait for homepage
    verify user not logged in   ${loginstatus}
    go to help page
    check auto login info

8. fill the form
    ${loginstatus}  wait for homepage
    verify user not logged in   ${loginstatus}
    go to help page
    go to form page
    check form page         # id & text values gonna be set

9. call now
    ${loginstatus}  wait for homepage
    verify user not logged in   ${loginstatus}
    go to help page
    check call now

10. get password
    ${loginstatus}  wait for homepage
    verify user not logged in   ${loginstatus}
    wait for homepage(not logged in)
    get password

11. supernet login
    ${loginstatus}  wait for homepage
    verify user not logged in   ${loginstatus}
    wait for homepage(not logged in)
    go to supernet login page
    check supernet login page

12. login with correct credentials
    ${loginstatus}  wait for homepage
    login           ${loginstatus}

13. login attempt with wrong number and password
    ${loginstatus}  wait for homepage
    login with wrong credentials    ${loginstatus}

14. login attempt with no password
    ${loginstatus}  wait for homepage
    login with no password      ${loginstatus}

15. login attempt with invalid number
    ${loginstatus}  wait for homepage
    login with invalid number   ${loginstatus}

16. remember me(checked)
    ${loginstatus}  wait for homepage
    remember me checked         ${loginstatus}

17. remember me(unchecked)
    ${loginstatus}  wait for homepage
    remember me unchecked   ${loginstatus}

18. options menu
    ${loginstatus}  wait for homepage
    verify user logged in   ${loginstatus}
    check options menu

19. campaigns
    ${loginstatus}  wait for homepage
    verify user logged in   ${loginstatus}
    open left menu
    go to campaigns
    check campaigns page

20. campaign detail
    ${loginstatus}  wait for homepage
    verify user logged in   ${loginstatus}
    open left menu
    go to campaigns
    go to campaign detail
    check campaign detail

21. mobile options
    ${loginstatus}  wait for homepage
    verify user logged in   ${loginstatus}
    open left menu
    go to mobile options
    check mobile options

22. internet packages
    ${loginstatus}  wait for homepage
    verify user logged in   ${loginstatus}
    open left menu
    go to mobile options
    go to internet packages
    check internet packages

23. internet package detail
    ${loginstatus}  wait for homepage
    verify user logged in   ${loginstatus}
    open left menu
    go to mobile options
    go to internet packages
    go to package detail    Aylik 1 GB Paketi
    check package detail

24. 4.5G services
    ${loginstatus}  wait for homepage
    verify user logged in   ${loginstatus}
    open left menu
    go to mobile options
    go to 4.5G services
    check 4.5G services

25. 4.5G services detail
    ${loginstatus}  wait for homepage
    verify user logged in   ${loginstatus}
    open left menu
    go to mobile options
    go to 4.5G services
    go to package detail    GüvenliNet Paketi
    check package detail

26. SMS packages
    ${loginstatus}  wait for homepage
    verify user logged in   ${loginstatus}
    open left menu
    go to mobile options
    go to SMS packages
    check SMS packages

27. SMS packages detail
    ${loginstatus}  wait for homepage
    verify user logged in   ${loginstatus}
    open left menu
    go to mobile options
    go to sms packages
    go to package detail    Her Yone Midi SMS Paketi
    check package detail

28. abroad packages
    ${loginstatus}  wait for homepage
    verify user logged in   ${loginstatus}
    open left menu
    go to mobile options
    go to abroad packages
    check abroad packages

29. abroad packages detail
    ${loginstatus}  wait for homepage
    verify user logged in   ${loginstatus}
    open left menu
    go to mobile options
    go to abroad packages
    go to package detail    25 MB Aylik Dunya Paketi
    check package detail

30. abroad call packages
    ${loginstatus}  wait for homepage
    verify user logged in   ${loginstatus}
    open left menu
    go to mobile options
    go to abroad call packages
    check abroad call packages

31. abroad call packages detail
    ${loginstatus}  wait for homepage
    verify user logged in   ${loginstatus}
    open left menu
    go to mobile options
    go to abroad call packages
    go to package detail    Dunya Kucuk Paketi
    check package detail

32. homepage control
    ${loginstatus}  wait for homepage
    verify user logged in   ${loginstatus}































