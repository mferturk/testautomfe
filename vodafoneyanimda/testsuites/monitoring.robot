*** Settings ***
Test Setup          starter
Test Teardown       close_app
Library             AppiumLibrary
#Library             AnywhereLibrary
#Library             FakerLibrary
Library             Collections
Library             String
Library             Screenshot
#Library             ../resources/externalKeywords.py
Resource            ../resources/mfeKeywords.robot
Resource            ../resources/vodafoneYanimda_functions.robot
Resource            ../resources/vodafoneYanimda_object_repository_android.robot
Resource            ../resources/vodafoneYanimda_object_repository_ios.robot
#Resource            ../resources/vodafoneYanimda_variables.py
Resource            ../resources/test_datas.robot


*** Test Cases ***
wifi login
    wait for homepage(not logged in)
    just login

cellular login
    ${loginstatus}  wait for homepage
    login   ${loginstatus}

homepage slider
    ${loginstatus}  wait for homepage
    verify user logged in   ${loginstatus}
    slider control

router trial
    #router keyword      wait until page does not contain element    progressBar     60
    #router keyword      wait until page contains element            btnhamburger    60
    #router keyword      click element       btnhamburger
    router keyword      wait and click element      btnhamburger    60
    sleep   10