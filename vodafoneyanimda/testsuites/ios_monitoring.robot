*** Settings ***
Test Setup          launch app with device id
Test Teardown       close_app
Library             AppiumLibrary
#Library             AnywhereLibrary
#Library             FakerLibrary
Library             Collections
Library             String
Library             Screenshot
#Library             ../resources/externalKeywords.py
Resource            ../resources/vodafoneYanimda_keywords.robot
Resource            ../resources/vodafoneYanimda_functions.robot
Resource            ../resources/vodafoneYanimda_object_repository_android.robot
Resource            ../resources/vodafoneYanimda_object_repository_ios.robot
#Resource            ../resources/vodafoneYanimda_variables.py
Resource            ../resources/test_datas.robot


*** Test Cases ***
Test title
