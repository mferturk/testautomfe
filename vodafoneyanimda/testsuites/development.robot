*** Settings ***
Documentation    Suite description
#Test Setup          launch app with device id
#Test Teardown       close_app
Library             AppiumLibrary
#Library             AnywhereLibrary
#Library             FakerLibrary
Library             Collections
Library             String
Library             Screenshot
#Library             ../resources/externalKeywords.py
Resource            ../resources/vodafoneYanimda_keywords.robot
Resource            ../resources/vodafoneYanimda_functions.robot
Resource            ../resources/vodafoneYanimda_object_repository_android.robot
Resource            ../resources/vodafoneYanimda_object_repository_ios.robot
#Resource            ../resources/vodafoneYanimda_variables.py
Resource            ../resources/test_datas.robot

*** Test Cases ***


#homepage
    #check permissions
    #login
    #wait for homepage
    #open left menu
    #verify campaign page

#experiments
    #sleep    5
    #log    naber
    #run keyword if    '${PLATFORM_NAME}'=='iOS'    log    this is ios device
    #run keyword if    '${PLATFORM_NAME}'=='Android'    log    this is Android device
    #open application  ${REMOTE_URL}    platformName=${PLATFORM_NAME}    udid=${DEVICE_UDID}    app=${APP}    deviceName=${DEVICE_NAME}    automationName=${AUTOMATION_NAME}
    #${i}    Generate Random String    1    [NUMBERS]
    #log    ${i}
    #${i}=    Evaluate    abs(${i})
    #${company}=


MyForLoop
    Log  In myforloop now.  WARN
    @{gvar}=  Create List    a  b  c
    Log  gvar is @{gvar} now.  WARN
|  | :FOR  | ${x} | in | @{gvar} |
|  |       | Log  | havealine ${x} now. | WARN |
|  | Log   | ending myforloop now.     | WARN |



evaluatedeneme
    set global variable    ${SCREEN_SIZE_X}         ${${DEVICE_ID}_SCREEN_SIZE_X}
    set global variable    ${SCREEN_SIZE_Y}         ${${DEVICE_ID}_SCREEN_SIZE_Y}
    deneme      0.3     0.7     0.3     0.2



*** keywords ***

deneme
    [arguments]  ${start_x_rate}    ${start_y_rate}     ${end_x_rate}   ${end_y_rate}
    log  ${screen_size_x}
    log  ${screen_size_y}
    log  ${start_x_rate}
    ${x1}=    evaluate    ${start_x_rate}*${SCREEN_SIZE_X}
    ${y1}=    evaluate    ${start_y_rate}*${SCREEN_SIZE_Y}
    ${x2}=    evaluate    ${end_x_rate}*${SCREEN_SIZE_X}
    ${y2}=    evaluate    ${end_y_rate}*${SCREEN_SIZE_Y}
    log     ${x1}
    log     ${y1}
    log     ${x2}
    log     ${y2}
