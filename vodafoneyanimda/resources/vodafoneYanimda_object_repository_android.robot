*** Variables ***
${btnPermissionAllow_android}            id=com.android.packageinstaller:id/permission_allow_button
${btnPermissionDeny_android}             id=com.android.packageinstaller:id/permission_deny_button
${btnAcceptUserAgreement_android}        id=com.vodafone.selfservis:id/btnAccept
${btnRejectUserAgreement_android}        id=com.vodafone.selfservis:id/btnReject
${tvScreenTitle_android}                 id=tvScreenTitle
${btnHamburger_android}                  id=com.vodafone.selfservis:id/btnToolbarAction
${btnCampaigns_android}                  id=com.vodafone.selfservis:id/btnCampaigns
${progressBar_android}                   id=android:id/progress
${btnPositive_android}                   id=button1
${btnNegative_android}                   id=button2
${btnPermissionAllow_android}            id=permission_allow_button
${btnPermissionDeny_android}             id=permission_deny_button
${loginUserName_android}                 id=com.vodafone.selfservis:id/etUsername
${loginPassword_android}                 id=com.vodafone.selfservis:id/etMhwp
${btnLogin_android}                      id=com.vodafone.selfservis:id/btnLogin
${btnLoginwithSuperNet_android}          id=com.vodafone.selfservis:id/btnLoginSuperNet
${rememberMe_android}                    id=com.vodafone.selfservis:id/cdRememberMe
${btnHelp_android}                       id=com.vodafone.selfservis:id/infoIV
${btnGetPassword_android}                id=com.vodafone.selfservis:id/btnSendMhwpSMS
${tvScreenTitle_android}                 id=com.vodafone.selfservis:id/tvScreenTitle
${bannerVodafone_android}                id=com.vodafone.selfservis:id/rlMNPBannerBox
${btnFreeUsageInfo_android}              id=com.vodafone.selfservis:id/btnFreeUsageInfo
${btnAutoLoginInfo_android}              id=com.vodafone.selfservis:id/btnAutologinInfo
${tvWidgetInfo_android}                  id=com.vodafone.selfservis:id/widgetInfoTV
${btnFillForm_android}                   id=com.vodafone.selfservis:id/rlMNPBannerAreaForm
${btnCallNow_android}                    id=com.vodafone.selfservis:id/rlMNPBannerAreaCall
${etFormName_android}                    id=fq529
${etFormSurname_android}                 id=fq530
${etFormNumber_android}                  id=fq531
${cbFormAgree_android}                   id=checkbox-agree
${btnFormSave_android}                   id=btnSave
${etFormName_android_text}                    Ad *
${etFormSurname_android_text}                 Soyad *
${etFormNumber_android_text}                  Cep Telefonu (5XXXXXXXXX) *
${etRequiredFields_android_text}              * zorunlu alanlar
${cbFormAgree_android_text}                   İhtiyacıma en uygun teklifin sunulması için, paylaştığım bilgilerin gerekli ölçüde ve yeterli süre boyunca işlenmesini ve Vodafone’un yetkilendireceği üçüncü parti iş ortakları ile paylaşılmasını kabul ediyorum.
${btnFormSave_android_text}                   Kaydet
${supernetLoginUsername_android}         id=com.vodafone.selfservis:id/etUsername
${supernetLoginPassword_android}         id=com.vodafone.selfservis:id/etMhwp
${btnSupernetLogin_android}              id=com.vodafone.selfservis:id/btnLogin
${btnReturnMainPage_android}             id=com.vodafone.selfservis:id/btnReturnMainPage
#${tvGetPasswordMessage_android_name}        Şifre almak için S yazıp 7000e ücretsiz mesaj atmanız yeterlidir. 7000 servisinden gelecek olan şifre giriş ekranına otomatik olarak girilecek ve uygulamaya girişiniz sağlanacaktır.
${rememberMeMessage_android}             Uygulamaya kolayca girmek için Beni Hatırla özelliğini aktif etmek ister misiniz?
${savetheNumber_android}                 'Hesabım' menüsünden istediğiniz zaman yeni bir numara ekleyebilir ya da kaydetmiş olduğunuz numaralarınızı silebilirsiniz.
${progressBar_android}                   id=android:id/progress
${tvHello_android}                       id=com.vodafone.selfservis:id/tvToolbarHello
${tvMsisdn_android}                      id=com.vodafone.selfservis:id/tvToolbarMsisdn
${tvToolBarTitle_android}                id=com.vodafone.selfservis:id/tvToolbarTitle
${btnLogout_android}                     id=com.vodafone.selfservis:id/btnLogout
${rememberMeCheckbox_android}            id=com.vodafone.selfservis:id/cdRememberMe
${tvSubscriberName_LM_android}           id=com.vodafone.selfservis:id/tvSubscriberName
${tvMsisdn_android}                      id=com.vodafone.selfservis:id/tvMsisdn
${btnHomePage_LM_android}                id=com.vodafone.selfservis:id/btnHome
${btnCampaigns_LM_android}               id=com.vodafone.selfservis:id/btnCampaigns
${btnUnbilledInvoice_LM_android}         id=com.vodafone.selfservis:id/btnUnbilledInvoice
${btnLiraTopup_LM_android}               id=com.vodafone.selfservis:id/btnLiraTopup
${btnMobileOptions_LM_android}           id=com.vodafone.selfservis:id/btnMobileOptions
${btnUserPlan_LM_android}                id=com.vodafone.selfservis:id/btnUserPlan
${btnAvailablePlans_LM_android}          id=com.vodafone.selfservis:id/btnAvailableTariffs
${btnFourG_LM_android}                   id=com.vodafone.selfservis:id/btnFourG
${btnAbroadGuide_LM_android}             id=com.vodafone.selfservis:id/btnAbroadGuide
${btnPackages_LM_android}                id=com.vodafone.selfservis:id/btnPackages
${btnBalance_LM_android}                 id=com.vodafone.selfservis:id/btnBalance
${btnEShop_LM_android}                   id=com.vodafone.selfservis:id/btnEShop
${btnLiraTransfer_android}               id=com.vodafone.selfservis:id/btnLiraTransfer
${btnLocalAccounts_LM_android}           id=com.vodafone.selfservis:id/btnLocalAccounts
${btnContentServices_LM_android}         id=com.vodafone.selfservis:id/btnContentServices
${btnSettings_LM_android}                id=com.vodafone.selfservis:id/btnSettings
${btnHelp_LM_android}                    id=com.vodafone.selfservis:id/btnHelp
${tvTimerforMondayMessage_android}       \#BanaNeVar diyenler için Pazartesi'ye çok az kaldı.
${timer_android}                         id=com.vodafone.selfservis:id/timerLL
${btnSeeLastGift_android}                id=com.vodafone.selfservis:id/buttonSeeLastGift
${tvMissedMondayMessage_android}         Ups! Pazartesi’yi kaçırdın, önümüzdeki Pazartesi Bana Ne Var hediyeni almak için tekrar bekleriz.
${tvUnexpectedError_android}             Beklenmedik bir hata oluÅ
${btnDetailInfoCampaigns_android}        id=com.vodafone.selfservis:id/rlInfoButton
${imgCampaignList_android}               id=com.vodafone.selfservis:id/iv_big
${tvCampaginDetailTitle_android}         id=com.vodafone.selfservis:id/tvTitle
${imgCampaignDetail_android}             id=com.vodafone.selfservis:id/campaignIV
${tvCampaignDetailDescription_android}   id=com.vodafone.selfservis:id/tvDescription
${btnSubscribeCampaign_android}          id=com.vodafone.selfservis:id/btnSubscribe
${btnUseCampaign_android}                id=com.vodafone.selfservis:id/btnUse
${btnCampaignUsageInfo_android}          id=com.vodafone.selfservis:id/btnUsageConditions
${tvInternetPackages_android}            İnternet Paketleri
${tv4.5GServices_android}                4.5G Servisleri
${tvMessagePackages_android}             Mesaj Paketleri
${tvAbroadPackages_android}              Yurtdışında Kullanım Paketleri
${tvAbroadCallPackages_android}          Yurtdışını Arama Paketleri
${tvOptionTitle_android}                 id=com.vodafone.selfservis:id/TvOptionTitle
${tvOptionPrice_android}                 id=com.vodafone.selfservis:id/tvOptionPrice
${tvPackageType_android}                 id=com.vodafone.selfservis:id/optionInfoDescTV
${tvOptionInfoDescription_android}       id=com.vodafone.selfservis:id/optionInfoDescriptionTV
${btnGetPackage_android}                 id=com.vodafone.selfservis:id/btnCancelPackage
${tvAmount_android}                      id=com.vodafone.selfservis:id/tvAmount
${imgArrow_android}                      id=com.vodafone.selfservis:id/imgActions
${imgTLicon_android}                     id=com.vodafone.selfservis:id/imgIconTL
${btnSeeGift_android}                    id=com.vodafone.selfservis:id/buttonSeeGift
${slider4.5G_android}                    4.5G SERVİSLER
${sliderCampaigns_android}               BANA NE VAR?
${sliderCall_android}                    KONUŞMA
${sliderInternet_android}                İNTERNET
${sliderMessage_android}                 MESAJLAŞMA
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}
${_android}


