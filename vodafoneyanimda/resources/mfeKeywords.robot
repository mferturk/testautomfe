*** Settings ***
Library           AppiumLibrary
Library           Collections
Library           String
Library           XML
Resource          vodafoneYanimda_functions.robot
Resource          vodafoneYanimda_object_repository_android.robot
Resource          vodafoneYanimda_object_repository_ios.robot
#Resource          vodafoneYanimda_variables.py
Resource          test_datas.robot
Library           Screenshot

*** Variables ***


*** Keywords ***

launch app with device id
    set global variable    ${REMOTE_URL}            ${${DEVICE_ID}_REMOTE_URL}
    set global variable    ${DEVICE_NAME}           ${${DEVICE_ID}_DEVICE_NAME}
    set global variable    ${DEVICE_DESCRIPTION}    ${${DEVICE_ID}_DEVICE_DESCRIPTION}
    set global variable    ${DEVICE_UDID}           ${${DEVICE_ID}_DEVICE_UDID}
    set global variable    ${APP}                   ${${DEVICE_ID}_APP}
    set global variable    ${PLATFORM_NAME}         ${${DEVICE_ID}_PLATFORM_NAME}
    set global variable    ${PLATFORM_VERSION}      ${${DEVICE_ID}_PLATFORM_VERSION}
    set global variable    ${AUTOMATION_NAME}       ${${DEVICE_ID}_AUTOMATION_NAME}
    set global variable    ${SCREEN_SIZE_X}         ${${DEVICE_ID}_SCREEN_SIZE_X}
    set global variable    ${SCREEN_SIZE_Y}         ${${DEVICE_ID}_SCREEN_SIZE_Y}
    log    ${DEVICE_ID}
    log    ${PLATFORM_NAME}
    log    ${DEVICE_UDID}
    log    ${PLATFORM_VERSION}
    open application    ${REMOTE_URL}    platformName=${PLATFORM_NAME}    udid=${DEVICE_UDID}    app=${APP}    deviceName=${DEVICE_NAME}    automationName=${AUTOMATION_NAME}      xcodeConfigFile=${XCODECONFIGUREPROFILE}     realDeviceLogger=${REAL_DEVICE_LOGGER}       usePrebuiltWDA=${USE_PREBUILT_WDA}     noReset=true
#    open application    ${REMOTE_URL}    platformName=${PLATFORM_NAME}    udid=${DEVICE_UDID}    app=${APP}    deviceName=${DEVICE_NAME}    automationName=${AUTOMATION_NAME}      xcodeConfigFile=${XCODECONFIGUREPROFILE}     realDeviceLogger=${REAL_DEVICE_LOGGER}       usePrebuiltWDA=${USE_PREBUILT_WDA}

close_app
    capture page screenshot
    close application

click on element
    [Arguments]    ${element}
    router keyword    click element    ${element}

wait for element
    [Arguments]    ${element}    ${timeout}=20 sec
    router keyword    wait until page contains element  ${element}  ${timeout}

wait for element to vanish
    [Arguments]    ${element}    ${timeout}=20 sec
    router keyword    wait until page does not contain element  ${element}  ${timeout}

wait and click element
    [Arguments]    ${element}    ${timeout}=30
    router keyword  wait until page contains element    ${element}    ${timeout}
    click on element    ${element}

click element if text exists
    [arguments]  ${text}    ${element}    ${status}=NULL
    ${status}    run keyword and return status    router keyword  page should contain text    ${text}
    log          ${status}
    run keyword if    '${status}'=='True'   log     related text found
    run keyword if    '${status}'=='True'   router keyword      click element    ${element}

#click text if exists
#    [arguments]  ${text1}   ${text2}=None   ${text3}=None
#    ${status}   ${value}=   run keyword and ignore error  page should contain text   ${text}
#    run keyword if  '${status}'=='True'     click text     ${text}

swiper
    [arguments]     ${start_x_rate}    ${start_y_rate}    ${end_x_rate}    ${end_y_rate}    ${duration}=1000
    ${x1}=    evaluate    ${start_x_rate}*${SCREEN_SIZE_X}
    ${y1}=    evaluate    ${start_y_rate}*${SCREEN_SIZE_Y}
    ${x2}=    evaluate    ${end_x_rate}*${SCREEN_SIZE_X}
    ${y2}=    evaluate    ${end_y_rate}*${SCREEN_SIZE_Y}
    swipe    ${x1}    ${y1}    ${x2}    ${y2}    ${duration}

swipe until click element
    [documentation]
    [arguments]     ${element}    ${start_x}    ${start_y}    ${end_x}    ${end_y}    ${max_swipe_count}=10
    ${failLimit}    Evaluate      ${max_swipe_count}-1
|   | :FOR | ${i} | IN RANGE | 1 | ${max_swipe_count} |
|   |      | ${status} | ${value}= | run keyword and ignore error | router keyword | click element | ${element} |
|   |      | log       | ${i}   |
|   |      | sleep     | 1      |
|   |      | Run Keyword If     | '${i}'=='${failLimit}'    | Fail      | ${element} could not clicked
|   |      | Run Keyword If     | '${status}'=='FAIL'       | swiper    | ${start_x}    | ${start_y} | ${end_x} | ${end_y} |
|   |      | Run Keyword If     | '${status}'=='PASS'       | capture page screenshot   |
|   |      | Exit For Loop If   | '${status}'=='PASS'

swipe until find element
    [arguments]     ${element}    ${start_x}    ${start_y}    ${end_x}    ${end_y}    ${max_swipe_count}=10
    ${failLimit}    Evaluate      ${max_swipe_count}-1
|   | :FOR | ${i} | IN RANGE | 1 | ${max_swipe_count} |
|   |      | ${status} | ${value}= | run keyword and ignore error |  router keyword | page should contain element | ${element} |
|   |      | log       | ${i}   |
|   |      | sleep     | 1      |
|   |      | Run Keyword If     | '${i}'=='${failLimit}'    | Fail      | ${element} could not found
|   |      | Run Keyword If     | '${status}'=='FAIL'       | swiper    | ${start_x}    | ${start_y} | ${end_x} | ${end_y} |
|   |      | Run Keyword If     | '${status}'=='PASS'       | capture page screenshot   |
|   |      | Exit For Loop If   | '${status}'=='PASS'

swipe until click text
    [documentation]
    [arguments]     ${text}    ${start_x}    ${start_y}    ${end_x}    ${end_y}    ${max_swipe_count}=10
    ${failLimit}    Evaluate      ${max_swipe_count}-1
|   | :FOR | ${i} | IN RANGE | 1 | ${max_swipe_count} |
|   |      | ${status} | ${value}= | run keyword and ignore error | click text | ${text} |
|   |      | log       | ${i}   |
|   |      | sleep     | 1      |
|   |      | Run Keyword If     | '${i}'=='${failLimit}'    | Fail      | ${text} could not clicked
|   |      | Run Keyword If     | '${status}'=='FAIL'       | swiper    | ${start_x}    | ${start_y} | ${end_x} | ${end_y} |
|   |      | Run Keyword If     | '${status}'=='PASS'       | capture page screenshot   |
|   |      | Exit For Loop If   | '${status}'=='PASS'

swipe until find text
    [arguments]     ${text}    ${start_x}    ${start_y}    ${end_x}    ${end_y}    ${max_swipe_count}=10
    ${failLimit}    Evaluate      ${max_swipe_count}-1
|   | :FOR | ${i} | IN RANGE | 1 | ${max_swipe_count} |
|   |      | ${status} | ${value}= | run keyword and ignore error | page should contain text | ${text} |
|   |      | log       | ${i}   |
|   |      | sleep     | 1      |
|   |      | Run Keyword If     | '${i}'=='${failLimit}'    | Fail      | ${text} could not found
|   |      | Run Keyword If     | '${status}'=='FAIL'       | swiper    | ${start_x}    | ${start_y} | ${end_x} | ${end_y} |
|   |      | Run Keyword If     | '${status}'=='PASS'       | capture page screenshot   |
|   |      | Exit For Loop If   | '${status}'=='PASS'

router keyword
    [Arguments]    ${keyword}    ${element}    ${2nd_argument}=None    ${3rd_argument}=None
    log  ${keyword}
    log  ${element}
    log  ${2nd_argument}
    log  ${3rd_argument}
    ${elementValue}    objectRepoWrapper    ${element}
    run keyword if      '${2nd_argument}'=='None'   ${keyword}    ${elementValue}
    run keyword if      '${2nd_argument}'!='None' and '${3rd_argument}'=='None'   ${keyword}    ${elementValue}    ${2nd_argument}
    run keyword unless  '${3rd_argument}'=='None'   ${keyword}    ${elementValue}    ${2nd_argument}    ${3rd_argument}

objectRepoWrapper
    [Arguments]    ${element}
    Run keyword if    '${PLATFORM_NAME}'=='Android'     Set Global Variable    ${elementValue}    ${element}_android
    Run keyword if    '${PLATFORM_NAME}'=='iOS'         Set Global Variable    ${elementValue}    ${element}_ios
    [Return]    ${${elementValue}}


close keyboard
    Run keyword if    '${PLATFORM_NAME}'=='Android'     hide keyboard
    Run keyword if    '${PLATFORM_NAME}'=='iOS'         click text  Bitti


click text if exists
    [Arguments]    ${text1}=    ${text2}=    ${text3}=    ${text4}=    ${text5}=    ${text6}=   ${text7}=    ${text8}=    ${text9}=    ${text10}=
    [Documentation]    Clicks on text if its text exists on current page
|    | :FOR | ${i} | IN RANGE | 1 | 10 |
|    |      | ${status}         | ${value}= | Run Keyword And Ignore Error  |   Page Should Contain Element |   name=${text${i}}
|    |      | Run keyword if    | '${status}'=='PASS'   | Click Element     |   name=${text${i}}            |
|    |      | Run keyword if    | '${status}'=='PASS'   | log               |   clicked on ${text${i}}      |
|    |      | Exit For Loop If  | '${status}'=='PASS'   |