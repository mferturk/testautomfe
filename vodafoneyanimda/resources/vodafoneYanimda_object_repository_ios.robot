*** Variables ***
#system elements
${progressBar_ios}                   Sürüyor
${notificationAllow_ios}             “Yanımda” Would Like to Send You Notifications
${cellularDataAllow_ios}             Veri işleme İzni
${accessLocationAllow_ios}           Allow “Yanımda” to access your location even when you are not using the app?
${btnAllowEn_ios}                    Allow
${btnDenyEn_ios}                     Don’t Allow
${btnApproveTr_ios}                  Kabul Et
${btnDenyTr_ios}                     Vazgeç
${btnAcceptUserAgreement_ios}        Onayla
${btnRejectUserAgreement_ios}        Reddet
${btnNegative_ios}                   Hayır
${btnPositive_ios}                   Evet

${btnHamburger_ios}                  ButtonIconLeftMenuRedSkin
${btnCampaigns_ios}                  id=com.vodafone.selfservis:id/btnCampaigns
${btnAndroidPositive_ios}            id=button1
${btnAndroidNegative_ios}            id=button2
${btnPermissionAllow_ios}            id=permission_allow_button
${btnPermissionDeny_ios}             id=permission_deny_button
${tvScreenTitle_ios}                 id=tvScreenTitle

#Loginscreen
${btnLogin_ios}                      Giriş yap
${btnLoginwithSuperNet_ios}          Vodafone SüperNet ile giriş yap
${rememberMe_ios}                    Beni hatırla
${loginUserName_ios}                 5xxxxxxxxx
${loginPassword_ios}                 Şifreniz

${splashImageSplash_ios}             id=com.vodafone.selfservis:id/imgRhombusSplash
${splashImageFull_ios}               id=com.vodafone.selfservis:id/imgRhombusFull
${btnHelp_ios}                       ButtonIconQuestion
${btnGetPassword_ios}                ButtonGetPassword
${tvScreenTitle_ios}                 id=com.vodafone.selfservis:id/tvScreenTitle
${bannerVodafone_ios}                id=com.vodafone.selfservis:id/rlMNPBannerBox
${btnFreeUsageInfo_ios}              id=com.vodafone.selfservis:id/btnFreeUsageInfo
${btnAutoLoginInfo_ios}              id=com.vodafone.selfservis:id/btnAutologinInfo
${tvWidgetInfo_ios}                  id=com.vodafone.selfservis:id/widgetInfoTV
${btnFillForm_ios}                   id=com.vodafone.selfservis:id/rlMNPBannerAreaForm
${btnCallNow_ios}                    id=com.vodafone.selfservis:id/rlMNPBannerAreaCall
${etFormName_ios}                    id=fq529
${etFormSurname_ios}                 id=fq530
${etFormNumber_ios}                  id=fq531
${cbFormAgree_ios}                   id=checkbox-agree
${btnFormSave_ios}                   id=btnSave
${etFormName_ios}                    Ad *
${etFormSurname_ios}                 Soyad *
${etFormNumber_ios}                  Cep Telefonu (5XXXXXXXXX) *
${etRequiredFields_ios}              * zorunlu alanlar
${cbFormAgree_ios}                   İhtiyacıma en uygun teklifin sunulması için, paylaştığım bilgilerin gerekli ölçüde ve yeterli süre boyunca işlenmesini ve Vodafone’un yetkilendireceği üçüncü parti iş ortakları ile paylaşılmasını kabul ediyorum.
${btnFormSave_ios}                   Kaydet
${supernetLoginUsername_ios}         id=com.vodafone.selfservis:id/etUsername
${supernetLoginPassword_ios}         id=com.vodafone.selfservis:id/etMhwp
${btnSupernetLogin_ios}              id=com.vodafone.selfservis:id/btnLogin
${btnReturnMainPage_ios}             id=com.vodafone.selfservis:id/btnReturnMainPage
#${tvGetPasswordMessage_ios_name}        Şifre almak için S yazıp 7000e ücretsiz mesaj atmanız yeterlidir. 7000 servisinden gelecek olan şifre giriş ekranına otomatik olarak girilecek ve uygulamaya girişiniz sağlanacaktır.
${rememberMeMessage_ios}             Uygulamaya kolayca girmek için Beni Hatırla özelliğini aktif etmek ister misiniz?
${savetheNumber_ios}                 'Hesabım' menüsünden istediğiniz zaman yeni bir numara ekleyebilir ya da kaydetmiş olduğunuz numaralarınızı silebilirsiniz.
${tvHello_ios}                       xpath=//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[2]/XCUIElementTypeStaticText[2]
${tvMsisdn_ios}                      xpath=//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[2]/XCUIElementTypeStaticText[3]
${tvToolBarTitle_ios}                Vodafone Yanımda
${btnLogout_ios}                     Çıkış
${rememberMeCheckbox_ios}            Beni hatırla
${tvSubscriberName_LM_ios}           id=com.vodafone.selfservis:id/tvSubscriberName
${btnHomePage_LM_ios}                Ana Sayfa
${btnCampaigns_LM_ios}               Bana Ne Var?
${btn4.5GServices_ios}               4.5G Servisler
${btnLiraTopup_LM_ios}               id=com.vodafone.selfservis:id/btnLiraTopup
${btnMobileOptions_LM_ios}           Ek Paket Al
${btnUserPlan_LM_ios}                Tarifem ve Paketlerim
${btnAvailablePlans_LM_ios}          Geçebileceğim Tarifeler
${btnFourG_LM_ios}                   ButtonIconLeftMenuFourG
${btnAbroadGuide_LM_ios}             Yurtdışı Rehberim
${btnPackages_LM_ios}                Kalan Kullanımlarım
${btnUnbilledInvoice_LM_ios}         Güncel Tutar
${btnBalance_LM_ios}                 Faturam
${btnPayBill_LM_ios}                 Fatura Ödeme
${btnEShop_LM_ios}                   Akıl Almaz Cihaz Teklifleri
${btnLiraTransfer_ios}               id=com.vodafone.selfservis:id/btnLiraTransfer
${btnLocalAccounts_LM_ios}           Hesabım
${btnContentServices_LM_ios}         İçerik Servisleri
${btnSettings_LM_ios}                Ayarlar
${btnHelp_LM_ios}                    Yardım ve Bize Ulaşın
${tvTimerforMondayMessage_ios}       \#BanaNeVar diyenler için Pazartesi'ye çok az kaldı.
${timer_ios}                         id=com.vodafone.selfservis:id/timerLL
${btnSeeLastGift_ios}                id=com.vodafone.selfservis:id/buttonSeeLastGift
${tvMissedMondayMessage_ios}         Ups! Pazartesi’yi kaçırdın, önümüzdeki Pazartesi Bana Ne Var hediyeni almak için tekrar bekleriz.
${tvUnexpectedError_ios}             Beklenmedik bir hata oluÅ
${btnDetailInfoCampaigns_ios}        id=com.vodafone.selfservis:id/rlInfoButton
${imgCampaignList_ios}               id=com.vodafone.selfservis:id/iv_big
${tvCampaginDetailTitle_ios}         id=com.vodafone.selfservis:id/tvTitle
${imgCampaignDetail_ios}             id=com.vodafone.selfservis:id/campaignIV
${tvCampaignDetailDescription_ios}   id=com.vodafone.selfservis:id/tvDescription
${btnSubscribeCampaign_ios}          id=com.vodafone.selfservis:id/btnSubscribe
${btnUseCampaign_ios}                id=com.vodafone.selfservis:id/btnUse
${btnCampaignUsageInfo_ios}          id=com.vodafone.selfservis:id/btnUsageConditions
${tvInternetPackages_ios}            İnternet Paketleri
${tv4.5GServices_ios}                4.5G Servisleri
${tvMessagePackages_ios}             Mesaj Paketleri
${tvAbroadPackages_ios}              Yurtdışında Kullanım Paketleri
${tvAbroadCallPackages_ios}          Yurtdışını Arama Paketleri
${tvOptionTitle_ios}                 id=com.vodafone.selfservis:id/TvOptionTitle
${tvOptionPrice_ios}                 id=com.vodafone.selfservis:id/tvOptionPrice
${tvPackageType_ios}                 id=com.vodafone.selfservis:id/optionInfoDescTV
${tvOptionInfoDescription_ios}       id=com.vodafone.selfservis:id/optionInfoDescriptionTV
${btnGetPackage_ios}                 id=com.vodafone.selfservis:id/btnCancelPackage
${tvAmount_ios}                      id=com.vodafone.selfservis:id/tvAmount
${imgArrow_ios}                      id=com.vodafone.selfservis:id/imgActions
${imgTLicon_ios}                     id=com.vodafone.selfservis:id/imgIconTL
${btnSeeGift_ios}                    id=com.vodafone.selfservis:id/buttonSeeGift
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}
${_ios}


