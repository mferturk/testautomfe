*** Variables ***

# login credentials
#${username0}	5413268528
#${password0}	36456500
#${username1}	5413268460
#${password1}	94380286
#${username2}	5413268463
#${password2}	12345678
#${username3}	5413268464
#${password3}	74453366
#${username4}	5413268466
#${password4}	93161275
#${username5}	5413268521
#${password5}	66219588
#${username6}	5413268522
#${password6}	44929195
#${username7}	5413268522
#${password7}	44929195
#${username8}	5413268522
#${password8}	44929195
#${username9}	5413268522
#${password9}	44929195

${username0}	5413268522
${password0}	44929195
${username1}	5413268522
${password1}	44929195
${username2}	5413268522
${password2}	44929195
${username3}	5413268522
${password3}	44929195
${username4}	5413268522
${password4}	44929195
${username5}	5413268522
${password5}	44929195
${username6}	5413268522
${password6}	44929195
${username7}	5413268522
${password7}	44929195
${username8}	5413268522
${password8}	44929195
${username9}	5413268522
${password9}	44929195


# Test Users Data
${phone}            5551234567
${pass}             1234
${msisdn}
${loginStatus}      ${EMPTY}

# Device Settings

# set device id to run on specific device settings
${DEVICE_ID}                m00

${NEW_COMMAND_TIMEOUT}      300
${AUTO_ACCEPT_ALERTS}       false
${AUTO_DISMISS_ALERTS}      false
${SEND_KEY_STRATEGY}        grouped

# Device and application settings (desired capabilities)

# ios
${REAL_DEVICE_LOGGER}       /Users/mferturk/node_modules/appium/build/deviceconsole/deviceconsole
${USE_PREBUILT_WDA}         true
${XCODECONFIGUREPROFILE}    /Users/magis/node-modules/appium/node-modules/appium-xcuitest-driver/WebDriverAgent/WebDriverAgent.xcodeproj

#${REMOTE_URL}               http://0.0.0.0:4723/wd/hub
#${DEVICE_NAME}              "magis's iPhone"
#${DEVICE_DESCRIPTION}       "iPhone 5"
#${DEVICE_UDID}              ca669c7d2cb281fe4c3de90a8366d4075edc5c6a
#${APP}                      /Users/magistechnology/Downloads/iphone_VodafoneMCare.app
#${PLATFORM_NAME}            iOS
#${PLATFORM_VERSION}         9.3.5
#${AUTOMATION_NAME}          XCUITest
#${xcodeConfigFile}          /Users/magistechnology/appium/node-modules/appium-xcuitest-driver/WebDriverAgent/WebDriverAgent.xcodeproj
#${REAL_DEVICE_LOGER}        /Users/magistechnology/node_modules/deviceconsole/deviceconsole
#${SCREENSHOT_URL}           /Users/magistechnology/PyCharmProjects/TestMobApps/screenshots

${m00_REMOTE_URL}           http://0.0.0.0:4723/wd/hub
${m00_DEVICE_NAME}          "mferturk's iPhone"
${m00_DEVICE_DESCRIPTION}   "iPhone 5"
${m00_DEVICE_UDID}          ca669c7d2cb281fe4c3de90a8366d4075edc5c6a
${m00_APP}                  /Users/mferturk/Documents/automation/mobile/vodafoneyanimda/executable/iphone_VodafoneMCare.app
${m00_PLATFORM_NAME}        iOS
${m00_PLATFORM_VERSION}     10.2
${m00_AUTOMATION_NAME}      XCUITest
${m00_SCREEN_SIZE_X}        320
${m00_SCREEN_SIZE_Y}        568

${m03_REMOTE_URL}           http://0.0.0.0:4723/wd/hub
${m03_DEVICE_NAME}          ""
${m03_DEVICE_DESCRIPTION}   "iPhone 6"
${m03_DEVICE_UDID}
${m03_APP}                  /Users/mferturk/Documents/automation/mobile/vodafoneyanimda/executable/yanimda.ipa
${m03_PLATFORM_NAME}        iOS
${m03_PLATFORM_VERSION}
${m03_AUTOMATION_NAME}      XCUITest
${m03_SCREEN_SIZE_X}        375
${m03_SCREEN_SIZE_Y}        667

${m04_REMOTE_URL}           http://0.0.0.0:4723/wd/hub
${m04_DEVICE_NAME}          ""
${m04_DEVICE_DESCRIPTION}   "iPhone 7"
${m04_DEVICE_UDID}
${m04_APP}                  /Users/mferturk/Documents/automation/mobile/vodafoneyanimda/executable/yanimda.ipa
${m04_PLATFORM_NAME}        iOS
${m04_PLATFORM_VERSION}
${m04_AUTOMATION_NAME}      XCUITest
${m04_SCREEN_SIZE_X}        375
${m04_SCREEN_SIZE_Y}        667

${m05_REMOTE_URL}           http://0.0.0.0:4723/wd/hub
${m05_DEVICE_NAME}          ""
${m05_DEVICE_DESCRIPTION}   "iPhone 6 Plus"
${m05_DEVICE_UDID}
${m05_APP}                  /Users/mferturk/Documents/automation/mobile/vodafoneyanimda/executable/yanimda.ipa
${m05_PLATFORM_NAME}        iOS
${m05_PLATFORM_VERSION}
${m05_AUTOMATION_NAME}      XCUITest
${m05_SCREEN_SIZE_X}        414
${m05_SCREEN_SIZE_Y}        736

${m06_REMOTE_URL}           http://0.0.0.0:4723/wd/hub
${m06_DEVICE_NAME}          ""
${m06_DEVICE_DESCRIPTION}   "iPhone 7 Plus"
${m06_DEVICE_UDID}
${m06_APP}                  /Users/mferturk/Documents/automation/mobile/vodafoneyanimda/executable/yanimda.ipa
${m06_PLATFORM_NAME}        iOS
${m06_PLATFORM_VERSION}
${m06_AUTOMATION_NAME}      XCUITest
${m06_SCREEN_SIZE_X}        414
${m06_SCREEN_SIZE_Y}        736



# android

${m01_REMOTE_URL}           http://0.0.0.0:4723/wd/hub
#${m01_REMOTE_URL}           http://0.0.0.0:4731/wd/hub
${m01_DEVICE_NAME}          "samsung galaxy s6"
${m01_DEVICE_DESCRIPTION}   "magis test device"
${m01_DEVICE_UDID}          06157df6bc9b911e
${m01_APP}                  /Users/mferturk/Documents/automation/mobile/vodafoneyanimda/executable/vodafoneyanimda_v4.1.1.com.apk
${m01_PLATFORM_NAME}        Android
${m01_PLATFORM_VERSION}     6.0.1
${m01_AUTOMATION_NAME}      appium
${m01_SCREEN_SIZE_X}        1440
${m01_SCREEN_SIZE_Y}        2560

${m02_REMOTE_URL}           http://0.0.0.0:4723/wd/hub
${m02_DEVICE_NAME}          "general mobile 4g"
${m02_DEVICE_DESCRIPTION}   "furkan's personal device"
${m02_DEVICE_UDID}          e2ad45d
${m02_APP}                  /Users/mferturk/Documents/automation/mobile/vodafoneyanimda/executable/yanimda.vodafone.selfservis.apk
${m02_PLATFORM_NAME}        Android
${m02_PLATFORM_VERSION}     7.1.1
${m02_AUTOMATION_NAME}      appium
${m02_SCREEN_SIZE_X}        720
${m02_SCREEN_SIZE_Y}        1280

${m07_REMOTE_URL}           http://0.0.0.0:4723/wd/hub
${m07_DEVICE_NAME}          "Samsung Galaxy J7"
${m07_DEVICE_DESCRIPTION}   "serturk's"
${m07_DEVICE_UDID}          33004c76ab659287
${m07_APP}                  /Users/mferturk/Documents/automation/mobile/vodafoneyanimda/executable/yanimda.vodafone.selfservis.apk
${m07_PLATFORM_NAME}        Android
${m07_PLATFORM_VERSION}     6.0.1
${m07_AUTOMATION_NAME}      appium
${m07_SCREEN_SIZE_X}        720
${m07_SCREEN_SIZE_Y}        1280

${m08_REMOTE_URL}           http://0.0.0.0:4723/wd/hub
${m08_DEVICE_NAME}          "Samsung Galaxy S6"
${m08_DEVICE_DESCRIPTION}   "ssiray's"
${m08_DEVICE_UDID}          04157df4e10f203d
${m08_APP}                  /Users/mferturk/Documents/automation/mobile/vodafoneyanimda/executable/yanimda.vodafone.selfservis.apk
${m08_PLATFORM_NAME}        Android
${m08_PLATFORM_VERSION}     6.0.1
${m08_AUTOMATION_NAME}      appium
${m08_SCREEN_SIZE_X}        1440
${m08_SCREEN_SIZE_Y}        2560

${m09_REMOTE_URL}           http://0.0.0.0:4723/wd/hub
${m09_DEVICE_NAME}          "Samsung Galaxy J5"
${m09_DEVICE_DESCRIPTION}   "usinergy's"
${m09_DEVICE_UDID}          dcd1dafb
${m09_APP}                  /Users/mferturk/Documents/automation/mobile/vodafoneyanimda/executable/yanimda.vodafone.selfservis.apk
${m09_PLATFORM_NAME}        Android
${m09_PLATFORM_VERSION}     6.0.1
${m09_AUTOMATION_NAME}      appium
${m09_SCREEN_SIZE_X}        720
${m09_SCREEN_SIZE_Y}        1280

${m10_REMOTE_URL}           http://0.0.0.0:4723/wd/hub
${m10_DEVICE_NAME}          "LG G Pro Lite"
${m10_DEVICE_DESCRIPTION}   "magis test device"
${m10_DEVICE_UDID}          QCGYIB6LC6SCBIKJ
${m10_APP}                  /Users/mferturk/Documents/automation/mobile/vodafoneyanimda/executable/yanimda.vodafone.selfservis.apk
${m10_PLATFORM_NAME}        Android
${m10_PLATFORM_VERSION}     4.4.2
${m10_AUTOMATION_NAME}      appium
${m10_SCREEN_SIZE_X}        540
${m10_SCREEN_SIZE_Y}        960

${m11_REMOTE_URL}           http://0.0.0.0:4723/wd/hub
${m11_DEVICE_NAME}          "LG G Pro Lite"
${m11_DEVICE_DESCRIPTION}   "magis test device"
${m11_DEVICE_UDID}          CQT4SOPFCYJRR88S
${m11_APP}                  /Users/mferturk/Documents/automation/mobile/vodafoneyanimda/executable/yanimda.vodafone.selfservis.apk
${m11_PLATFORM_NAME}        Android
${m11_PLATFORM_VERSION}     4.2.1
${m11_AUTOMATION_NAME}      appium
${m11_SCREEN_SIZE_X}        540
${m11_SCREEN_SIZE_Y}        960