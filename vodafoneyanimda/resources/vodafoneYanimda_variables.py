
### General Variables ###

#Selenium Grid
NEW_COMMAND_TIMEOUT="300"
AUTO_ACCEPT_ALERTS="false"
AUTO_DISMISS_ALERTS="false"
SEND_KEY_STRATEGY="grouped"

# Device and application settings (desired capabilities)

# set device id to run on specific device settings
DEVICE_ID="m1"

# ios

#${REMOTE_URL}               http://0.0.0.0:4723/wd/hub
#${DEVICE_NAME}              "magis's iPhone"
#${DEVICE_DESCRIPTION}       "iPhone 5"
#${DEVICE_UDID}              ca669c7d2cb281fe4c3de90a8366d4075edc5c6a
#${APP}                      /Users/magistechnology/Downloads/iphone_VodafoneMCare.app
#${PLATFORM_NAME}            iOS
#${PLATFORM_VERSION}         9.3.5
#${AUTOMATION_NAME}          XCUITest
#${xcodeConfigFile}          /Users/magistechnology/appium/node-modules/appium-xcuitest-driver/WebDriverAgent/WebDriverAgent.xcodeproj
#${REAL_DEVICE_LOGER}        /Users/magistechnology/node_modules/deviceconsole/deviceconsole
#${SCREENSHOT_URL}           /Users/magistechnology/PyCharmProjects/TestMobApps/screenshots

m3_REMOTE_URL="http://0.0.0.0:4723/wd/hub"
m3_DEVICE_NAME="magis's iPhone"
m3_DEVICE_DESCRIPTION="iPhone 5"
m3_DEVICE_UDID_m3="5aaed5f1b71bdce9364b52247db3ec788e2452df"
m3_APP="/Users/mferturk/Downloads/Yanimda.ipa"
m3_PLATFORM_NAME="iOS"
m3_PLATFORM_VERSION="10.2"
m3_AUTOMATION_NAME="XCUITest"
m3_xcodeConfigFile="/Users/mferturk/appium/node-modules/appium-xcuitest-driver/WebDriverAgent/WebDriverAgent.xcodeproj"

# android

m1_REMOTE_URL="http://0.0.0.0:4723/wd/hub"
m1_DEVICE_NAME="samsung"
m1_DEVICE_DESCRIPTION="ebea"
m1_DEVICE_UDID="06157df6bc9b911e"
m1_APP="/Users/mferturk/Documents/automation/mobile/vodafoneyanimda/executable/yanimda.vodafone.selfservis.apk"
m1_PLATFORM_NAME="Android"
m1_PLATFORM_VERSION="6.0.1"
m1_AUTOMATION_NAME="appium"

m2_REMOTE_URL="http://0.0.0.0:4723/wd/hub"
m2_DEVICE_NAME="general mobile 4g"
m2_DEVICE_DESCRIPTION_m2="ebea"
m2_DEVICE_UDID_m2="e2ad45d"
m2_APP_m2="/Users/mferturk/Documents/automation/mobile/vodafoneyanimda/executable/yanimda.vodafone.selfservis.apk"
m2_PLATFORM_NAME="Android"
m2_PLATFORM_VERSION="7.1.1"
m2_AUTOMATION_NAME="appium"
