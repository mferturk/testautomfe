*** Settings ***

Library         AppiumLibrary
Library         Collections
Library         String
Library         Screenshot
Library         XML
Resource        mfeKeywords.robot
Resource        vodafoneYanimda_functions.robot
Resource        vodafoneYanimda_object_repository_android.robot
Resource        vodafoneYanimda_object_repository_ios.robot
Resource        test_datas.robot


*** Keywords ***

starter
    launch app with device id
    run keyword if  '${PLATFORM_NAME}'=='Android'    check permissions
    run keyword if  '${PLATFORM_NAME}'=='iOS'        give permissions

splash control
    wait until page contains element  ${splashimagesplash_android_id}
    wait until page contains element  ${splashimagefull_android_id}

reject user agreement
    wait until page contains    Kullanım Koşulları  1
    capture page screenshot
    click element      ${btnrejectuseragreement_android_id}
    page should not contain element  ${applayout}

accept user agreement
    wait until page contains    Kullanım Koşulları  1
    capture page screenshot
    click element      ${btnacceptuseragreement_android_id}
    wait until page contains    Veri kullanım izni  1

allow phone calls
    click element if text exists   Yanımda uygulamasına telefon aramaları yapma ve çağrıları yönetme izni verilsin mi?      ${btnpermissionallow_android_id}
    wait until page contains    Allow Yanımda to make and manage phone calls?   1
    capture page screenshot
    click element      ${btnpermissionallow_android_id}

allow access media
    click element if text exists    Yanımda uygulamasına cihazınızdaki fotoğraflara, medyaya ve dosyalara erişme izni verilsin mi?   ${btnpermissionallow_android_id}
    wait until page contains    Allow Yanımda to access photos, media, and files on your device?    1
    capture page screenshot
    click element      ${btnpermissionallow_android_id}

accept cellular data
    wait until page contains    Veri kullanım izni  1
    capture page screenshot
    click element      ${btnAndroidPositive_android_id}

allow access location
    click element if text exists    Yanımda uygulamasına bu cihazın konumuna erişme izni verilsin mi?   ${btnpermissionallow_android_id}
    wait until page contains    Allow Yanımda to access this device's location?     1
    capture page screenshot
    click element      ${btnpermissionallow_android_id}

give permissions
    run keyword and ignore error    wait until page contains    Allow
    run keyword and ignore error    click text                  Allow
    run keyword and ignore error    wait until page contains    Kabul Et
    run keyword and ignore error    click text                  Kabul Et
    run keyword and ignore error    wait until page contains    Allow
    run keyword and ignore error    click text                  Allow
    run keyword and ignore error    wait until page contains    1-GENEL AÇIKLAMALAR
    run keyword and ignore error    click text                  1-GENEL AÇIKLAMALAR
    run keyword and ignore error    wait until page contains    Onayla
    run keyword and ignore error    click text                  Onayla
    run keyword and ignore error    wait until page contains    Yeni numara ile giriş yap
    run keyword and ignore error    click text                  Yeni numara ile giriş yap

check permissions
    #[Arguments]    ${locator}    ${timeout}
    run keyword and ignore error    page should contain text    DAHA SONRA HATIRLAT
    run keyword and ignore error    click text      DAHA SONRA HATIRLAT
    run keyword and ignore error    allow phone calls
    run keyword and ignore error    allow access media
    run keyword and ignore error    accept user agreement
    run keyword and ignore error    accept cellular data
    run keyword and ignore error    allow access location

check login page
    wait until page contains        Güvenli giriş
    page should contain element     ${loginusername_android_id}
    page should contain element     ${loginpassword_android_id}
    page should contain element     ${btngetpassword_android_id}
    page should contain element     ${rememberme_android_id}
    page should contain element     ${btnlogin_android_id}
    page should contain element     ${btnsupernetlogin_android_id}
    capture page screenshot

go to help page
    wait and click element          ${btnhelp_android_id}
    wait until page contains element  ${tvscreentitle_android_id}   10
    capture page screenshot

check help page
    page should contain text        Nasıl giriş yapmak istersiniz?
    page should contain element     ${btnfreeusageinfo_android_id}
    page should contain element     ${btnautologininfo_android_id}
    page should contain element     ${bannervodafone_android_id}
    page should contain element     ${tvwidgetinfo_android_id}
    page should contain text        Vodafone Yanımda Widget’ı şimdi yükleyin ve kalan kullanımlarınızı daha da kolay takip edin.
    capture page screenshot

check free usage info
    wait and click element          ${btnfreeusageinfo_android_id}
    wait until page contains element  ${tvscreentitle_android_id}
    page should contain text        Ücretsiz kullanım
    page should contain text        Vodafone Yanımda Uygulaması ücretsiz bir uygulamadır* ve kullanımı ücretsizdir.**
    page should contain text        * Vodafone Yanımda Uygulaması'nı uygulama marketinden indirmek için kullanacağınız internet bağlantısı mevcut internet tarifeniz üzerinden ücretlendirilir.
    page should contain text        ** Vodafone Cep Merkezi arama menüsünde kullanılan 3. parti uygulamasından gelen haritanın kullanacağı internet bağlantısı mevcut internet tarifenizden ücretlendirilir.
    page should contain text        Vodafone Yanımda Uygulaması’nın yurtdışında kullanımında internet ücretlendirmesi bulunduğunuz ülkeye ve kullandığınız operatöre göre değişiklik gösterebilir.
    page should contain text        kıllı telefonlar (iPhone,Android, Windows Mobile, Symbian, Palm, BlackBerry vb. işletim sistemine sahip), Tablet PC'ler (iPad,Samsung Galaxy vb.) ve Windows, Mac OS, Linux gibi işletim sistemlerine sahip cihazlar üzerindeki üretici tarafından yüklenmiş uygulamalar ve sonradan kullanıcı tarafından yüklenmiş uygulamalar ile bunların otomatik versiyon güncellemeleri internet kullanımı yaratabilir. İnternet paketiniz var ise söz konusu kullanımlar bu paketten düşer, yoksa standart internet tarifesi üzerinden ücretlendirilir.
    capture page screenshot

check auto login info
    wait and click element          ${btnautologininfo_android_id}
    wait until page contains element    ${tvscreentitle_android_id}
    page should contain text        Şifresiz Giriş
    page should contain text        Vodafone Yanımdaya şifresiz giriş yapmak çok kolay!
    page should contain text        Tek yapmanız gereken internet bağlantınızı Vodafone şebekesi üzerinden sağlamak!
    page should contain text        Üstelik Vodafone Yanımda Uygulamasına yurt içinden bağlandığınızda internet ücreti ödemezsiniz.
    page should contain text        İnternet bağlantınızı kablosuz ağ veya diğer operatörler üzerinden sağlıyorsanız:
    page should contain text        Vodafone Yanımda Uygulamasına girmeden önce şifre girmeniz gerekir. Şifre almak için S yazıp 7000e ücretsiz göndermeniz yeterli.
    page should contain text        BlackBerry bağlantı noktası (APN) üzerinden internet bağlantısı gerçekleştiren abonelerimiz şu an için Vodafone Yanımda şifreleri ile Vodafone Yanımda Uygulaması'na giriş yapabilirler.
    capture page screenshot

go to form page
    click element                   ${btnfillform_android_id}

check form page
    wait until page contains element    ${etformname_android_id}  20
    wait until page contains            ${etformname_android_text}
    page should contain element         ${etformsurname_android_id}
    page should contain element         ${etformnumber_android_id}
    page should contain element         ${cbformagree_android_id}
    page should contain element         ${btnformsave_android_id}
    page should contain text            ${etformsurname_android_text}
    page should contain text            ${etformnumber_android_text}
    page should contain text            ${cbformagree_android_text}
    page should contain text            ${btnformsave_android_text}
    capture page screenshot

check call now
    click element                   ${btncallnow_android_id}
    page should contain text        Vodafone'a geçmek için 0542 542 36 36'yı aramak ister misiniz?
    page should contain element     ${btnandroidpositive_android_id}
    page should contain element     ${btnandroidnegative_android_id}

get password
    click element                   ${btngetpassword_android_id}
    wait until page contains        Şifre almak için S yazıp 7000e ücretsiz mesaj atmanız yeterlidir. 7000 servisinden gelecek olan şifre giriş ekranına otomatik olarak girilecek ve uygulamaya girişiniz sağlanacaktır.
    wait until element is visible   ${btnandroidpositive_android_id}
    wait until element is visible   ${btnandroidnegative_android_id}
    click element                   ${btnandroidpositive_android_id}
    capture page screenshot

go to supernet login page
    click element                   ${btnloginwithsupernet_android_id}
    wait until element is visible   ${btnreturnmainpage_android_id}

check supernet login page
    page should contain element     ${supernetloginusername_android_id}
    page should contain element     ${supernetloginpassword_android_id}
    page should contain element     ${btngetpassword_android_id}
    page should contain element     ${rememberme_android_id}
    page should contain text        Kullanıcı Kodu faturanızın sol üstünde bulunan 10 haneli numaradır. Bu kodu 0850 542 1 542 ‘yi arayarak da öğrenebilirsiniz.

login
    #[arguments]  ${username}    ${password}
    [arguments]  ${loginstatus}
    log     ${loginStatus}
    run keyword if  '${loginStatus}'=='loggedin'    logout
    just login

just login
#create random credentials
    ${i}    Generate Random String    1    [NUMBERS]
    log  ${i}
    router keyword    input text        loginusername    ${username${i}}
    router keyword    input password    loginpassword    ${password${i}}
    capture page screenshot
    log  username: ${username${i}} and password: ${password${i}}
    close keyboard
    run keyword if  '${PLATFORM_NAME}'=='Android'   click on element    btnlogin
    run keyword if  '${PLATFORM_NAME}'=='Android'   wait for element to vanish      progressbar     20
    run keyword and ignore error    click on element    btnnegative
    click element if text exists    remembermemessage     btnNegative
    run keyword if  '${PLATFORM_NAME}'=='Android'   wait for element to vanish      progressbar     20
    unexpected error exception
    click element if text exists    savethenumber         btnNegative
    run keyword if  '${PLATFORM_NAME}'=='Android'   wait for element to vanish      progressbar     20
    unexpected error exception
    run keyword and ignore error    click text      Bir daha gösterme
#msisdn
    @{chars}=      split string to characters     ${username${i}}
    log  @{chars}[0]
    ${msisdn}=  catenate    (@{chars}[0]@{chars}[1]@{chars}[2]) @{chars}[3]@{chars}[4]@{chars}[5]-@{chars}[6]@{chars}[7]-@{chars}[8]@{chars}[9]
    log     ${msisdn}
#verify login process
    check homepage elements
    page should contain text            ${msisdn}
    capture page screenshot

check homepage elements
    wait for element            btnhamburger
    wait for element            tvmsisdn
    wait for element            tvtoolbartitle

wait for homepage
    [return]    ${loginstatus}
    ${status1}   ${value}=  run keyword and ignore error    wait until page contains element      ${btnhelp_android_id}   3
    ${status2}   ${value}=  run keyword and ignore error    wait until page contains element      ${tvhello_android_id}   3
    ${loginStatus}=         set variable if     '${status1}' == 'PASS' and '${status2}'=='FAIL'   notloggedin   loggedin
    log     ${loginStatus}
    capture page screenshot

wait for homepage(logged in)
    wait until page contains element  ${btnhamburger_android_id}
    log    homepage is viewed (logged in)
    Capture Page Screenshot

wait for homepage(not logged in)
    wait for element  btnhelp   45
    log    homepage is viewed (not logged in)
    Capture Page Screenshot

check login status
    [return]   ${loginStatus}
    ${loginStatus}=     run keyword and return status    page should contain element    ${tvhello_android_id}

logout
    open left menu
    swipe until click element
    swipe   445   1950    445   1000
    click element   ${btnlogout_android_id}
    wait and click element      ${btnandroidpositive_android_id}

open left menu
    Click Element    ${btnHamburger_android_id}
    capture page screenshot

verify campaign page
    click element    ${btnCampaigns_android_id}
    wait until element is visible    ${progressBar_android_id}
    Capture Page Screenshot
    wait until keyword succeeds  page should not contain element  progressBar
    sleep    1
    Page Should Contain Text    Bu Haftaki Hediyemi Gör
    Capture Page Screenshot

login with wrong credentials
    [arguments]  ${loginstatus}
    log                                         ${loginStatus}
    run keyword if                              '${loginStatus}'=='loggedin'    logout
    input text                                  ${loginusername_android_id}    5123456789
    input password                              ${loginpassword_android_id}    12345
    run keyword and ignore error                hide keyboard
    capture page screenshot
    click element                               ${btnlogin_android_id}
    wait until page does not contain element    ${progressbar_android_id}     20
#close warnings
    click element if text exists    ${remembermemessage_android_id}     ${btnandroidpositive_android_id}
    click element if text exists    ${savethenumber_android_id}         ${btnandroidpositive_android_id}
    page should contain text                    Giriş yaptığınız eski şifreniz hatalıdır. Lütfen tekrar deneyiniz.
    #page should contain text                    Kullanıcı adı veya şifreniz hatalıdır. Lütfen kontrol edip tekrar giriş yapınız.
    wait and click element                      ${btnandroidpositive_android_id}
    capture page screenshot

login with no password
    [arguments]  ${loginstatus}
    log                                         ${loginStatus}
    run keyword if                              '${loginStatus}'=='loggedin'    logout
    input text                                  ${loginusername_android_id}    5123456789
    capture page screenshot
    click element                               ${btnlogin_android_id}
    page should contain text                    Lütfen şifrenizi girin
    wait and click element                      ${btnandroidpositive_android_id}
    capture page screenshot

login with invalid number
    [arguments]  ${loginstatus}
    log                                         ${loginStatus}
    run keyword if                              '${loginStatus}'=='loggedin'    logout
    input password                              ${loginpassword_android_id}    12345
    capture page screenshot
    click element                               ${btnlogin_android_id}
    page should contain text                    Girdiğiniz numara geçersiz. Numara 5 ile başlamalı ve 10 basamaklı olmaldır
    capture page screenshot
    wait and click element                      ${btnandroidpositive_android_id}

remember me checked
    [arguments]  ${loginstatus}
    log     ${loginStatus}
    run keyword if  '${loginStatus}'=='loggedin'    logout
#create random credentials
    ${i}    Generate Random String    1    [NUMBERS]
    log  ${i}
    input text    ${loginusername_android_id}    ${username${i}}
    input password    ${loginpassword_android_id}    ${password${i}}
    capture page screenshot
    log  username: ${username${i}} and password: ${password${i}}
    click element   ${btnlogin_android_id}
    page should contain text     ${remembermemessage_android_id}
    page should contain element     ${btnandroidpositive_android_id}
    page should contain element     ${btnandroidnegative_android_id}

remember me unchecked
    [arguments]  ${loginstatus}
    log     ${loginStatus}
    run keyword if  '${loginStatus}'=='loggedin'    logout
#create random credentials
    ${i}    Generate Random String    1    [NUMBERS]
    log  ${i}
    input text          ${loginusername_android_id}    ${username${i}}
    input password       ${loginpassword_android_id}    ${password${i}}
    capture page screenshot
    log  username: ${username${i}} and password: ${password${i}}
    hide keyboard
    click element                               ${rememberme_android_id}
    click element                               ${btnlogin_android_id}
    page should not contain text                ${remembermemessage_android_id}
    wait until page does not contain element    ${progressbar_android_id}       20
    page should contain text                    ${savethenumber_android_id}
    page should contain element                 ${btnandroidpositive_android_id}
    page should contain element                 ${btnandroidnegative_android_id}
    capture page screenshot

check options menu
    open left menu
    swipe until find element    ${tvsubscribername_lm_android_id}       0.3    0.76    0.3    0.39
    swipe until find element    ${tvmsisdn_android_id}                  0.3    0.76    0.3    0.39
    swipe until find element    ${btnhomepage_lm_android_id}            0.3    0.76    0.3    0.39
    swipe until find element    ${btncampaigns_lm_android_id}           0.3    0.76    0.3    0.39
    #swipe until find element    ${btnliratopup_lm_android_id}           0.3    0.76    0.3    0.39
    swipe until find element    ${btnmobileoptions_lm_android_id}       0.3    0.76    0.3    0.39
    swipe until find element    ${btnuserplan_lm_android_id}            0.3    0.76    0.3    0.39
    swipe until find element    ${btnavailableplans_lm_android_id}      0.3    0.76    0.3    0.39
    swipe until find element    ${btnfourg_lm_android_id}               0.3    0.76    0.3    0.39
    swipe until find element    ${btnAbroadGuide_LM_android_id}         0.3    0.76    0.3    0.39
    swipe until find element    ${btnpackages_lm_android_id}            0.3    0.76    0.3    0.39
    #swipe until find element    ${btnbalance_lm_android_id}             0.3    0.76    0.3    0.39
    #swipe until find element    ${btnliratransfer_android_id}           0.3    0.76    0.3    0.39
    swipe until find element    ${btnlocalaccounts_lm_android_id}       0.3    0.76    0.3    0.39
    swipe until find element    ${btnContentServices_LM_android_id}     0.3    0.76    0.3    0.39
    swipe until find element    ${btnsettings_lm_android_id}            0.3    0.76    0.3    0.39
    swipe until find element    ${btnhelp_lm_android_id}                0.3    0.76    0.3    0.39
    swipe until find element    ${btnlogout_android_id}                 0.3    0.76    0.3    0.39

verify user logged in
    [arguments]  ${loginStatus}
    run keyword if  '${loginStatus}'=='notloggedin'  just login
    run keyword if  '${loginStatus}'=='loggedin'  log   user has already logged in
    capture page screenshot

verify user not logged in
    [arguments]     ${loginstatus}
    run keyword if  '${loginstatus}'=='loggedin'    logout
    run keyword if  '${loginStatus}'=='notloggedin'  log   user not logged in
    capture page screenshot

go to campaigns
    swipe until click element                   ${btncampaigns_lm_android_id}   0.3     0.76    0.3     0.39
    wait until page does not contain element    ${progressbar_android_id}       20
    capture page screenshot

go to campaign detail
    swipe until click element                   ${btndetailinfocampaigns_android_id}    0.5     0.8     0.5    0.6
    wait until page does not contain element    ${progressbar_android_id}               10
    capture page screenshot

check campaigns page
    ${status}   ${value}=   run keyword and ignore error    page should contain element     ${btnseegift_android_id}
    run keyword if      '${status}'=='FAIL'     check timer
    #run keyword if      '${status}'=='PASS'

check timer
    page should contain element                 ${timer_android_id}
    page should contain text                    ${tvtimerformondaymessage_android_id}
    page should contain element                 ${btndetailinfocampaigns_android_id}
    capture page screenshot
    wait and click element                      ${btnseelastgift_android_id}
    wait until page does not contain element    ${progressbar_android_id}
    ${status}   ${value}=   run keyword and ignore error    page should contain text     ${tvmissedmondaymessage_android_id}
    run keyword if   '${status}'=='FAIL'   page should contain text             Bu Haftaki Hediyem
    run keyword if   '${status}'=='FAIL'   page should not contain element      ${btnseelastgift_android_id}

go to topup
    swipe until click element   ${btnliratopup_lm_android_id}   0.3     0.76    0.3     0.39
    wait until page does not contain element  ${progressbar_android_id}     20
    capture page screenshot

go to mobile options
    swipe until click element   ${btnmobileoptions_lm_android_id}   0.3     0.76    0.3     0.39
    wait until page does not contain element  ${progressbar_android_id}     25
    capture page screenshot

unexpected error exception
    ${status}   ${value}=   run keyword and ignore error    page should contain text    ${tvunexpectederror_android}
    run keyword if     '${status}'=='PASS'   capture page screenshot
    run keyword if     '${status}'=='PASS'   fail    Unexpected error occured

check campaign detail
    swipe until find element     ${tvcampagindetailtitle_android_id}        0.5     0.8     0.5    0.6
    swipe until find element     ${imgcampaigndetail_android_id}            0.5     0.8     0.5    0.6
    #swipe until find element     ${tvcampaigndetaildescription_android_id}  0.5     0.8     0.5    0.6
    #swipe until find element     ${btnsubscribecampaign_android_id}         0.5     0.8     0.5    0.6
    swipe until find element     ${btncampaignusageinfo_android_id}         0.5     0.8     0.5    0.6
    swipe until find element     ${btnusecampaign_android_id}               0.5     0.8     0.5    0.6
    capture page screenshot

check mobile options
    page should contain text     ${tvinternetpackages_android_id}
    page should contain text     ${tvmessagepackages_android_id}
    page should contain text     ${tvabroadcallpackages_android_id}
    page should contain text     ${tvabroadpackages_android_id}
    page should contain text     ${tv4.5gservices_android_id}
    capture page screenshot

go to internet packages
    swipe until click text                      ${tvinternetpackages_android_id}    0.5     0.8     0.5    0.6
    wait until page does not contain element    ${progressbar_android_id}           10
    capture page screenshot

go to internet package detail
    swipe until click text                      Aylik 1 GB Paketi   0.5     0.8     0.5    0.6
    wait until page does not contain element    ${progressbar_android_id}   10
    capture page screenshot

check internet packages
    page should contain text    Bir ust tarife ile 4.5Gyi deneyimlemeniz icin ek 2GB
    page should contain text    4.5Gyi deneyimlemeniz için ek 2GB
    page should contain text    Sosyal Medya Paketi 1GB
    page should contain text    Video Paketi
    page should contain text    Netflix
    page should contain text    Gunluk 1GB Ek Paketi
    swipe until find text       Aylik 1GB Ek Internet Paketi    0.5     0.8     0.5    0.6
    swipe until find text       Spotify Aylik 500MB Paketi      0.5     0.8     0.5    0.6
    swipe until find text       Aylik 250MB Paketi              0.5     0.8     0.5    0.6
    swipe until find text       Aylik 1 GB Paketi               0.5     0.8     0.5    0.6

check package detail
    page should contain element         ${tvoptiontitle_android_id}
    page should contain element         ${tvoptionprice_android_id}
    page should contain element         ${tvoptioninfodescription_android_id}
    page should contain element         ${tvpackagetype_android_id}
    wait and click element              ${btngetpackage_android_id}
    wait until page contains            Talebiniz gerçekleştirilecektir, onaylıyor musunuz?
    wait until page contains element    ${btnandroidpositive_android_id}
    wait until page contains element    ${btnandroidnegative_android_id}

go to 4.5G services
    swipe until click text                      ${tv4.5GServices_android_id}    0.5     0.8     0.5    0.6
    wait until page does not contain element    ${progressbar_android_id}   10
    capture page screenshot

check 4.5G services
    page should contain text        GüvenliNet Paketi
    #page should contain text        Güvenli Depo 50GB Paketi
    #page should contain text        Güvenli Depo 100GB Paketi
    #page should contain text        Güvenli Depo 1TB Paketi
    page should contain element     ${tvcampagindetailtitle_android_id}
    page should contain element     ${imgtlicon_android_id}
    page should contain element     ${imgArrow_android_id}
    page should contain element     ${tvamount_android_id}

#check 4.5G services detail
#    page should contain element         ${tvoptiontitle_android_id}
#    page should contain element         ${tvoptionprice_android_id}
#    page should contain element         ${imgtlicon_android_id}
#    page should contain element         ${tvoptioninfodescription_android_id}
#    page should contain element         ${tvpackagetype_android_id}
#    wait and click element              ${btngetpackage_android_id}
#    wait until page contains            Talebiniz gerçekleştirilecektir, onaylıyor musunuz?
#    wait until page contains element    ${btnandroidpositive_android_id}
#    wait until page contains element    ${btnandroidnegative_android_id}

go to SMS packages
    swipe until click text                      ${tvmessagepackages_android_id}     0.5     0.8     0.5    0.6
    wait until page does not contain element    ${progressbar_android_id}   20
    capture page screenshot

go to package detail
    [arguments]  ${packageName}
    swipe until click text                      ${packageName}   0.5     0.8     0.5    0.6
    wait until page does not contain element    ${progressbar_android_id}   20
    capture page screenshot

check SMS packages
    page should contain text        SMS Avantaj 100 Paketi
    page should contain text        Her Yone Midi SMS Paketi
    page should contain text        Vodafonelularla Maksi SMS Paketi
    page should contain text        SMS Avantaj 11000 Paketi
    page should contain element     ${tvcampagindetailtitle_android_id}
    page should contain element     ${imgtlicon_android_id}
    page should contain element     ${imgArrow_android_id}
    page should contain element     ${tvamount_android_id}

go to abroad packages
    swipe until click text                      ${tvabroadpackages_android_id}     0.5     0.8     0.5    0.6
    wait until page does not contain element    ${progressbar_android_id}   20
    capture page screenshot

check abroad packages
    swipe until find text           Hersey Dahil Pasaport Paketi    0.5     0.8     0.5    0.6
    swipe until find text           Gunluk 25MB Large Paketi        0.5     0.8     0.5    0.6
    swipe until find text           30DK Small Paketi               0.5     0.8     0.5    0.6
    swipe until find text           60DK Small Paketi               0.5     0.8     0.5    0.6
    swipe until find text           Dunya 60 Paketi                 0.5     0.8     0.5    0.6
    swipe until find text           Avrupa, Amerika, Rusya 120 Dakika Paketi    0.5     0.8     0.5    0.6
    swipe until find text           25 MB Aylik Dunya Paketi        0.5     0.8     0.5    0.6
    swipe until find text           120 Dakika Tum Ulkeler Paketi   0.5     0.8     0.5    0.6
    swipe until find text           50 MB Aylik Dunya Paketi        0.5     0.8     0.5    0.6
    page should contain element     ${tvcampagindetailtitle_android_id}
    page should contain element     ${imgtlicon_android_id}
    page should contain element     ${imgArrow_android_id}
    page should contain element     ${tvamount_android_id}

go to abroad call packages
    swipe until click text                      ${tvabroadcallpackages_android_id}     0.5     0.8     0.5    0.6
    wait until page does not contain element    ${progressbar_android_id}   20
    capture page screenshot

check abroad call packages
    swipe until find text   Dunya Kucuk Paketi       0.5     0.8     0.5    0.6
    page should contain element     ${tvcampagindetailtitle_android_id}
    page should contain element     ${imgtlicon_android_id}
    page should contain element     ${imgArrow_android_id}
    page should contain element     ${tvamount_android_id}

slider control
    click text      ${slider4.5g_android_id}
    page should not contain text    ${slidercall_android_id}
    click text      ${slidercampaigns_android_id}}
    page should contain text    ${slidercall_android_id}
    click text      ${slidercall_android_id}
    page should not contain text    ${slider4.5G_android_id}
    click text      ${sliderinternet_android_id}
    page should not contain text    ${slidercampaigns_android_id}
    click text      ${slidermessage_android_id}
    page should not contain text    ${slidercall_android_id}






















